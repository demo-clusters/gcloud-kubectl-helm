FROM node:alpine

RUN apk --update --no-cache add python openssl openssh bash ca-certificates libc6-compat curl

# google cloud sdk
ENV GCLOUD_VERSION="185.0.0"
# RUN wget https://dl.google.com/dl/cloudsdk/channels/rapid/google-cloud-sdk.zip && unzip google-cloud-sdk.zip && rm google-cloud-sdk.zip
RUN wget https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz && tar xzf google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz && rm google-cloud-sdk-${GCLOUD_VERSION}-linux-x86_64.tar.gz
RUN google-cloud-sdk/install.sh --usage-reporting=true --path-update=true --bash-completion=true --rc-path=/.bashrc --additional-components kubectl

# Finalize
RUN mkdir /.ssh
ENV PATH $PATH:/google-cloud-sdk/bin:/usr/local/bin

# Note: Latest version of helm may be found at:
# https://github.com/kubernetes/helm/releases
ENV HELM_VERSION="v2.8.0"
ENV FILENAME="helm-${HELM_VERSION}-linux-amd64.tar.gz"

RUN curl -L http://storage.googleapis.com/kubernetes-helm/${FILENAME} -o /tmp/${FILENAME} \
  && tar -zxvf /tmp/${FILENAME} -C /tmp \
  && mv /tmp/linux-amd64/helm /usr/local/bin/helm
